const Hotel = require('../data/hotel')

class HotelService {
    static save(hotel) {
        console.log(hotel)
        let newHotel = new Hotel({
            name: hotel.name,
            description: hotel.description, 
            //location: hotel.location,
            images: hotel.images,
            links: hotel.links,
            averagePrice: hotel.averagePrice
        })
        console.log(newHotel)
        newHotel.save( (err) => {
            if(err) {
                return handleError(err)
            }
            console.log('hotel saved')
        })
    }
}


module.exports = HotelService