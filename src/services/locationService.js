const Location = require('../data/location')

class LocationService {
    static save(location) {
        console.log(location)
        let newLocation = new Location({
            city: location.city,
            country: location.country, 
            description: location.description,
            //location: location.location,
            images: location.images,
            hotels: location.hotels,
            attractions: location.attractions,
            averageTemperature: location.averageTemperature
        })
        console.log(newLocation)
        newLocation.save( (err) => {
            if(err) {
                return handleError(err)
            }
            console.log('location saved')
        })
    }
}


module.exports = LocationService