const express = require('express')
const router = express.Router()
const  HotelController  = require('./controllers/hotelController')
const hotelController = new HotelController()
const LocationController  = require('./controllers/locationController')
const locationController = new LocationController()


router.get('/', (req, res) => {
    res.send({"oi":"oi"})
})

router.post('/hotel', (req, res) => {
    hotelController.save(req.body)
    res.status(200).json({"msg":"Hotel added."})
})

router.post('/location', (req, res) => {
    locationController.save(req.body)
    res.status(200).json({"msg":"Location added."})
})

module.exports = router