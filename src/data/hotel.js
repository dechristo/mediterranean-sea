const mongoose = require('mongoose')
const Location = require('../models/location')

const HotelSchema = mongoose.Schema({
    name: {
        type: String,
        require: true    
    },
    description: {
        type: String,
        require: true
    },
    averagePrice: Number,
    location:{
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Location',
    },
    links: Array,
    images: Array
})

const Hotel = mongoose.model('Hotel', HotelSchema)
module.exports = Hotel