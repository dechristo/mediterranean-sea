const mongoose = require('mongoose')

const LocationSchema = mongoose.Schema({
    country: {
        type: String,
        require: true
    },
    city: {
        type: String,
        require: true
    },
    coordinates: Array,
    description: {
        type: String,
        require: true
    },
    images: Array,
    averageTemperature : String,
    attractions: Array,
    hotels: Array,
    attractions: String
})

const Location = mongoose.model('location', LocationSchema)
module.exports = Location