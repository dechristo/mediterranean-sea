class Location {
    constructor(country, city, coordinates, description, images, averageTemperature, hotels, attractions) {
        this.country = country
        this.city = city
        this.coordinates = coordinates
        this.description = description
        this.images = images
        this.averageTemperature = averageTemperature
        this.hotels = hotels
        this.attractions = attractions
    }

    get country() {
        return this.country
    }

    set country(country) {
        this.country = this.country
    }

    get city() {
        return this.city
    }

    set city(city) {
        this.city = city
    }

    get coordinates() {
        return this.coordinates
    }

    set coordinates(coordinates) {
        this.coordinates = coordinates
    }

    get description() {
        return this.description
    }

    set description(description) {
        this.description = description
    }

    get name() {
        return this.name
    }

    set name(name) {
        this.name = name
    }

    get hotels() {
        return this.hotels
    }

    set hotels(hotels) {
        this.hotels = hotels
    }

    get averageTemperature() {
        return this.averageTemperature
    }

    set averageTemperature(averageTemperature) {
        this.averageTemperature = averageTemperature
    }

    get attractions() {
        return thi.attractions
    }

    set attractions(attractions) {
        this.attractions = attractions
    }
}

module.exports = Location