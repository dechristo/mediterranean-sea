class Coordinates {
    constructor(latitude, longitude) {
        this._latitude = latitude,
        this._longitude = longitude
    }

    get latitude() {
        return this._latitude
    }

    set latitude(latitude) {
        this._latitude
    }

    get longitude() {
        return this._longitude
    }

    set longitude(longitude) {
        this._longitude = longitude
    }
}

module.exports = Coordinates