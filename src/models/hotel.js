class Hotel {
    constructor(
        name,
        description,
        location,
        links,
        images,
        averagePrice
    ) {
        this.name = name
        this.description = description
        this.location = location
        this.links = links
        this.images = images
        this.averagePrice = averagePrice
    }

    get name() {
        return this.name
    }

    set name(name) {
        this.name = name
    }

    get description() {
        return this.description
    }

    set description(description) {
        this.description = description
    }

    get location() {
        return this.location
    } 

    set location(location) {
        this.location = location
    }

    get links() {
        return this.links
    }

    set links(links) {
        this.links = links
    }

    get images() {
        return this.images
    }

    set images(images) {
        this.images = images
    }

    get averagePrice() {
        return this.averagePrice
    }

    set averagePrice(averagePrice) {
        this.averagePrice = averagePrice
    }
}

module.exports = Hotel