const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const router = require ('./src/router')
const mongoose = require ('mongoose')

mongoose.connect('mongodb://localhost/mediterranean');
app.use(bodyParser.json())
app.use('/api', router)

app.listen(3000, () => {
    console.log('server started at 3000...')
})